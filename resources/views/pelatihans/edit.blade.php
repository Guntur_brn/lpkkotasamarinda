@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Tambah Pelatihan</h6>
                    </div>
                    <form action="{{ route('pelatihan.update',$pelatihan->id) }}" method="POST" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-sm">
                                <label>Nama Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="nm_pelatihan" value="{{ $pelatihan->nm_pelatihan }}" >

                                <?php $pengajars = DB::table('pengajars')
                        ->get(); ?>


                                <label>Nama Pengajar</label>
                                <select class="form-control mb-3" name="kd_pengajar">
                                    @foreach ($pengajars as $pengajar)
                                        <?php if($pengajar->id = $pelatihan->kd_pengajar){ ?>
                                        <option value="{{ $pengajar->id }}" selected>{{ $pengajar->nm_pengajar }}</option>
                                        <?php } else { ?>
                                        <option value="{{ $pengajar->id }}">{{ $pengajar->nm_pengajar }}</option>
                                        <?php } ?>
                                    @endforeach
                                </select>

                                <label>Biaya Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan" value="{{ $pelatihan->biaya_pelatihan }}" >
                            </div>
                            <div class="col-sm">
                                <label>Jadwal</label>
                                <input type="date" class="form-control mb-3" placeholder="jadwal" name="jadwal" value="{{ $pelatihan->jadwal }}">
                                
                                <label>Gambar</label>
                                <input type="file" name="image" class="form-control" placeholder="image" >
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection