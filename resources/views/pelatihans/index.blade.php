@extends('layouts.user_type.auth')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="container">
                            <div class="row mt-4">
                                <div class="col-sm">
                                    <h6>Daftar Pelajaran </h6>
                                </div>
                                <div class="col-sm-auto">
                                    <a class="btn btn-dark" href="{{ route('pelatihan.create') }}">&plus; Tambah
                                        Pelajaran</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center mb-0">
                                    <thead>
                                        <tr>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Pelatihan</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Pengajar</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Biaya</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Pengaturan</th>
                                            <th class="text-secondary opacity-7"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($pelatihans as $pelatihan)
                                            <tr>
                                                <td>
                                                    <div class="d-flex px-2 py-1">
                                                        <div>
                                                            <img src="/image/{{ $pelatihan->image }}"
                                                                class="avatar avatar-sm me-3" alt="user1">
                                                        </div>
                                                        <div class="d-flex flex-column justify-content-center">
                                                            <h6 class="mb-0 text-sm">{{ $pelatihan->nm_pelatihan }}</h6>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">{{ $pelatihan->nm_pengajar }}
                                                    </p>
                                                </td>
                                                <td class="align-middle text-center text-sm">
                                                    <p class="text-xs font-weight-bold mb-0">
                                                        {{ $pelatihan->biaya_pelatihan }}</p>
                                                </td>
                                                <td class="align-middle text-center">
                                                    <form action="{{ route('pelatihan.destroy', $pelatihan->id) }}"
                                                        method="POST">
                                                        <a class="btn btn-primary shadow-none text-dark"
                                                            href="{{ route('pelatihan.edit', $pelatihan->id) }}"
                                                            style="width:60px;">Edit</a>

                                                        @csrf
                                                        @method('DELETE')

                                                        <button type="submit"
                                                            class=" btn btn-primary shadow-none text-danger"
                                                            style="margin-left:20px;">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="12" class="text-danger text-center align-middle py-3 px-5">
                                                    Tidak ada data
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
