@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Tambah Pelatihan</h6>
                    </div>
                    <form action="{{ route('pelatihan.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm">
                                <label>Nama Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="nm_pelatihan">

                                <label>Nama Pengajar</label>
                                <!-- <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="nm_pengajar"> -->
                                <select class="form-control mb-3" name="kd_pengajar">
                                    @foreach ($pengajars as $pengajar)
                                        <option value="{{ $pengajar->id }}">{{ $pengajar->nm_pengajar }}</option>
                                    @endforeach
                                </select>

                                <label>Biaya Pelatihan</label>
                                
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan">
                            </div>
                            <div class="col-sm">
                                <label>Jadwal</label>
                                <input type="date" class="form-control mb-3" placeholder="jadwal" name="jadwal" readonly>
                                
                                <label>Gambar</label>
                                <input type="file" name="image" class="form-control" placeholder="image">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection