@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Nilai Kelas</h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelajar</th>
                      <th class="text-left ml-2 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nilai</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $jadwals = DB::table('pelatihans') 
                        ->join('jadwals', 'pelatihans.id', '=', 'jadwals.kd_pelatihan')
                        ->where('kd_pelatihan','=',$kelasPengajar)
                        ->get(); ?>

                    @foreach ($jadwals as $jadwal)
                    <!-- {{ $jadwal->sub_pelatihan}} -->
                    <?php $pelatihans = DB::table('nilais') 
                        ->join('jadwals', 'nilais.kd_sub', '=', 'jadwals.id')
                        ->join('pelajars', 'nilais.kd_pelajar', '=','pelajars.kd_pelajar')
                        ->where('sub_pelatihan','=',$jadwal->sub_pelatihan)
                        ->select('nilais.*','pelajars.nm_pelajar')
                        // ->select('nilais.*')
                        ->get(); ?>
                    @foreach ($pelatihans as $pelatihan)
                    <form action="{{ route('nilai.update',$pelatihan->id) }}" method="POST" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')
                            <tr>
                        <td>
                        {{ $jadwal->sub_pelatihan}}
                            <div class="d-flex px-2 py-1">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pelatihan->nm_pelajar }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                        <input type="tel" class="form-control mb-3 float-center" placeholder="00" name="nilai" id="nilai" value="{{ $pelatihan->nilai }}" style="width:50px;">
                        </td>
                        <td class="align-middle text-center text-sm">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Ubah</button>
                        </td>
                        </tr>
                    </form>
                    @endforeach
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection