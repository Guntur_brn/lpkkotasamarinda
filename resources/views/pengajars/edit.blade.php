@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Tambah Pengajar</h6>
                    </div>
                    <form action="{{ route('pengajar.update',$pengajar->id) }}" method="POST" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-sm">
                                <label>Nama Pengajar</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pengajar" name="nm_pengajar" value="{{ $pengajar->nm_pengajar }}">

                                <label>Alamat</label>
                                <textarea type="text" class="form-control mb-3"  name="alamat" rows="4" >{{ $pengajar->alamat}}</textarea>
                                
                                <label>Kota</label>
                                <input type="text" class="form-control mb-3" placeholder="Kota" name="kota" value="{{ $pengajar->kota}}">

                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control mb-3" placeholder="Tempat Lahir" name="tmpt_lahir" value="{{ $pengajar->tmpt_lahir}}">
                            </div>
                            <div class="col-sm">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control mb-3" placeholder="Tanggal Lahir" name="tgl_lahir" value="{{ $pengajar->tgl_lahir}}">

                                <label>Jenis kelamin</label>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm">
                                            <?php if($pengajar->jns_kelamin=="L"){?>
                                            <input type="radio" id="L" name="jns_kelamin" value="L" checked><?php } else {?>
                                            <input type="radio" id="L" name="jns_kelamin" value="L" > <?php } ?>
                                            <label for="L">Laki - Laki</label><br>
                                        </div>
                                        <div class="col-sm">
                                            <?php if($pengajar->jns_kelamin=="P"){?>
                                            <input type="radio" id="P" name="jns_kelamin" value="P" checked><?php } else {?>
                                            <input type="radio" id="P" name="jns_kelamin" value="P" > <?php } ?>
                                            <label for="L">Perempuan</label><br>
                                        </div>
                                        <div class="col-sm">
                                        </div>
                                    </div>
                                </div>

                                <label>No Telepon</label>
                                <input type="tel" class="form-control mb-3" placeholder="No Telepon" name="no_telp" value="{{ $pengajar->no_telp}}">
                                
                                <label>Gambar</label><br>
                                <img src="/image/{{ $pengajar->image }}" width="300px">
                                <input type="file" name="image" class="form-control" placeholder="image">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Ubah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection