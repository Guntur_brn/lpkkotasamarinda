@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Daftar Pelajaran </h6>
                    </div>
                    <div class="col-sm-auto">
                        <a class="btn btn-dark"  href="{{ route('jadwal.show',$pelatihan) }}">&plus; Tambah Jadwal</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Jadwal</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Materi</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $pelatihans = DB::table('jadwals')
                        ->join('pelatihans', 'jadwals.kd_pelatihan', '=', 'pelatihans.id')                        
                        ->where('pelatihans.id', '=', $pelatihan)
                        ->select('jadwals.id','pelatihans.image','jadwals.sub_pelatihan','jadwals.jadwal_sub','jadwals.materi')
                        ->get(); ?>

                    @foreach ($pelatihans as $pelatihana)
                        
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                <img src="/image/{{ $pelatihana->image }}" class="avatar avatar-sm me-3" alt="user1">
                            </div>
                            </div>
                        </td>
                        <form action="{{ route('kelasPengajar.update',$pelatihana->id) }}" method="POST" enctype="multipart/form-data"> 
                        @csrf
                        @method('PUT')

                        <td>
                          <input type="text" class="form-control " placeholder="Tanggal Lahir" value="{{$pelatihana->sub_pelatihan }}"name="sub_pelatihan">
                        </td>
                        <td>
                          <input type="date" class="form-control " placeholder="Tanggal Lahir" value="{{$pelatihana->jadwal_sub }}"name="jadwal_sub">
                        </td>
                        
                        <td>
                          <input type="file" class="form-control " placeholder="Tanggal Lahir" value="{{$pelatihana->materi }}"name="materi">
                        </td>
                        <td class="align-middle text-center">
                          <a href="/file/{{ $pelatihana->materi }}"  class="btn btn-dark w-100 mt-4 mb-4 mb-0" download>Download Materi</a>
                        </td>
                        <td class="align-middle text-center">
                          <?php if($pelatihana->jadwal_sub == '' or $pelatihana->materi == '') {                        
                        ?>
                          <button type="submit" class="btn btn-dark w-100 mb-0">Upload</button>
                          <?php } else {}?>
                        </td>
                        </form>
                        </tr>
                          
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection