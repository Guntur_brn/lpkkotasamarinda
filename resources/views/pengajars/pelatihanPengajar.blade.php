@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Tambah Jadwal Pelajaran </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $pelatihans = DB::table('pelatihans')
                        // ->join('jadwals','pelatihans.id','=','jadwals.kd_pelatihan')
                        ->where('kd_pengajar','=',auth()->user()->kd_pengajar )
                        ->get(); 
                        ?>

                    @foreach ($pelatihans as $pelatihan)
                        <?php if($pelatihan->jadwal==null){
                            ?>
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                <img src="/image/{{ $pelatihan->image }}" class="avatar avatar-sm me-3" alt="user1">
                            </div>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pelatihan->nm_pelatihan }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center">
                            <a class="text-xs font-weight-bold mb-0" href="{{  route('kelasPengajar.edit',$pelatihan->id) }}" >Atur Jadwal</a>
                        </td>
                        </tr>
                        <?php } ?>
                          
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection