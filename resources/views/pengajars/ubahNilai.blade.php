@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Daftar Pelajaran </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Jadwal</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $pelatihans = DB::table('pelatihans') 
                        ->where('kd_pengajar','=',auth()->user()->id)
                        ->get(); ?>

                    @foreach ($pelatihans as $pelatihan)
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pelatihan->nm_pelatihan }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center text-sm">
                        <h6 class="mb-0 text-sm">{{ $pelatihan->jadwal }}</h6>
                        </td>
                        <td class="align-middle text-center">
                            <a class="text-xs font-weight-bold mb-0" href="{{ route('kelasPengajar.show',$pelatihan->id) }}" >Lihat Kelas</a>
                        </td>
                        </tr>
                          
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection