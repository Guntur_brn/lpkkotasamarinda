@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Pembuatan Akun Pengajar</h6>
                    </div>

                    <?php $kd_pengajars = DB::table('pengajars')
                    ->latest()
                    ->limit(1)
                        ->get(); ?>
              <form action="{{ route('pengajarAkun.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm">
                            <div class="mb-3">
                                    <label class="mt-3">Kd Pengajar</label>
                        @foreach ($kd_pengajars as $kd_pengajar)
                                    <input type="text" class="form-control" placeholder="Name" name="kd_pengajar" id="kd_pengajar"
                                        aria-label="Name" aria-describedby="name" value="{{ $kd_pengajar->id+1 }}" readonly>
                                @endforeach
                                </div>
                @csrf
                <div class="mb-3">
                  <input type="text" class="form-control" placeholder="Name" name="name" id="name" aria-label="Name" aria-describedby="name" value="{{ old('name') }}">
                  @error('name')
                    <p class="text-danger text-xs mt-2">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-3">
                  <input type="email" class="form-control" placeholder="Email" name="email" id="email" aria-label="Email" aria-describedby="email-addon" value="{{ old('email') }}">
                  @error('email')
                    <p class="text-danger text-xs mt-2">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-3">
                  <input type="password" class="form-control" placeholder="Password" name="password" id="password" aria-label="Password" aria-describedby="password-addon">
                  @error('password')
                    <p class="text-danger text-xs mt-2">{{ $message }}</p>
                  @enderror
                </div>
                <div class="mb-3">
                  <input type="text"  id="level" name="level" value="pengajar"  readonly class="form-control" placeholder="Password" name="password" id="password" aria-label="Password" aria-describedby="password-addon">
                  @error('password')
                    <p class="text-danger text-xs mt-2">{{ $message }}</p>
                  @enderror
                </div>
                <div class="form-check form-check-info text-left">
                  <input class="form-check-input" type="checkbox" name="agreement" id="flexCheckDefault" checked>
                  <label class="form-check-label" for="flexCheckDefault">
                    Saya setuju dengan <a href="javascript:;" class="text-dark font-weight-bolder">Syarat dan Ketentuan yg Berlaku</a>
                  </label>
                  @error('agreement')
                    <p class="text-danger text-xs mt-2">Setujui semua syarat dan ketentuan terleih dahulu.</p>
                  @enderror
                </div>
                <div class="text-center">
                  <button type="submit"class="btn bg-gradient-dark w-100 my-4 mb-2">Daftar</button>
                </div>
              </form>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection