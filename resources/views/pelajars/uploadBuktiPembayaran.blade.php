@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Upload Bukti Pembayaran</h6>
                    </div>

                    <?php $pembayarans = DB::table('pembayarans') 
                        ->where('id','=',$dashboard)
                        ->get(); ?>
                        
                    @foreach ($pembayarans as $pembayaran)
                    {{$pembayaran->id}}
                    <form action="{{ route('pelajarPembayaran.update', $pembayaran->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <label>Gambar</label><br>
                    <input type="file" name="image" class="form-control" placeholder="images">
                    <div class="text-center">
                        <button type="submit" id="upload" name="status" value="upload" class=" btn btn-dark w-100 mt-4 mb-0" >BENAR</button>
                    </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</main>

@endsection