@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Tambah Pelajar</h6>
                    </div>
                    <?php
        $kd_pengajars = DB::table('users')
                    ->latest()
                    ->limit(1)
                        ->get(); ?>

                    <form action="{{ route('pelajar.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm">
                            @foreach ($kd_pengajars as $kd_pengajar)
                        
                            <label>Kd Pelajar</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelajar" name="kd_pelajar" value="{{$kd_pengajar->id}}" readonly>
                        
                        
                                <label>Nama Pelajar</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelajar" name="nm_pelajar" value="{{$kd_pengajar->name}}">
                                @endforeach
                                <label>Alamat</label>
                                <textarea type="text" class="form-control mb-3" placeholder="Alamat" name="alamat" rows="4"></textarea>
                                
                                <label>Kota</label>
                                <input type="text" class="form-control mb-3" placeholder="Kota" name="kota">

                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control mb-3" placeholder="Tempat Lahir" name="tmpt_lahir">
                            </div>
                            <div class="col-sm">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control mb-3" placeholder="Tanggal Lahir" name="tgl_lahir">

                                <label>Jenis kelamin</label>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm">
                                            <input type="radio" id="L" name="jns_kelamin" value="L">
                                            <label for="L">Laki - Laki</label><br>
                                        </div>
                                        <div class="col-sm">
                                            <input type="radio" id="P" name="jns_kelamin" value="P">
                                            <label for="P">Perempuan</label>
                                        </div>
                                        <div class="col-sm">
                                        </div>
                                    </div>
                                </div>

                                <label>No Telepon</label>
                                <input type="tel" class="form-control mb-3" placeholder="No Telepon" name="no_telp">
                                
                                <label>Gambar</label>
                                <input type="file" name="image" class="form-control" placeholder="image">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Daftar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</main>

@endsection