@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Tambah Pelatihan Saya</h6>
                    </div>
                    <?php $pelatihans = DB::table('pelatihans')
                        ->join('pengajars', 'pelatihans.kd_pengajar','=','pengajars.id')
                        ->select('pelatihans.*','pengajars.nm_pengajar')
                        ->where('pelatihans.id', '=', $pelatihan)
                        ->get(); ?>

                    @foreach ($pelatihans as $pembayaran)

                    <form action="{{ route('kelasPelajar.store',$pembayaran->id) }}" method="POST" enctype="multipart/form-data"> 
                    @csrf
                        <div class="row">
                            <div class="col-sm">
                                <label>Nama Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="nm_pelatihan" value="{{ $pembayaran->nm_pelatihan }}" readonly>

                                <label>Nama Pengajar</label>
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan" value="{{ $pembayaran->nm_pengajar }}" readonly>

                                <label>Biaya Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan" value="{{ $pembayaran->biaya_pelatihan }}" readonly>
                            </div>
                            <div class="col-sm">
                                <label>Jadwal</label>
                                <input type="date" class="form-control mb-3" placeholder="jadwal" name="jadwal" value="{{ $pembayaran->jadwal }}" readonly>
                                
                                <label>Gambar</label>
                                <input type="file" name="image" class="form-control mb-3" placeholder="image" readonly>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pelatihan" value="{{ $pembayaran->id }}" readonly>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pelajar" value="{{ auth()->user()->id }}" readonly>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pengajar" value="{{$pembayaran->kd_pengajar }}" readonly>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="status" value="gagal" readonly>
                            </div>
                        </div>
                        <!-- {{auth()->user()->id}} -->
                        <!-- {{$pelatihan}} -->
                        <?php $cek_data = DB::table('pembayarans')
                        ->join('pelatihans', 'pembayarans.kd_pelatihan','=','pelatihans.id')
<<<<<<< HEAD
                        ->join('pelajars', 'pembayarans.kd_pelajar','=','pelajars.id')
                        ->where('pelajars.id', '=', auth()->user()->id)
=======
                        ->join('pelajars', 'pembayarans.kd_pelajar','=','pelajars.kd_pelajar')
                        ->where('pelajars.kd_pelajar', '=', auth()->user()->id)
>>>>>>> 740cc167410b68f4844385af0f4d50612c964346
                        ->where('pelatihans.id', '=', $pelatihan)
                        ->get(); 
                        // echo $cek_data;
                        if(count($cek_data)>0){
                            echo('
                            <a href="/file/'.$pembayaran->materi.'"  class="btn btn-dark w-100 mt-4 mb-4 mb-0" download>Download Materi</a>
                            <button type="readonly" class="btn btn-dark w-100  mb-0" disabled>Anda telah mengambil kelas ini</button>') ;
                        } else {
                            echo('
                            <div class="text-center">
                            <button type="submit" class="btn btn-dark w-100 mt-4 mb-0">Tambah Kelas</button>
                        </div>
                            ');
                        }
                        
                        ?>
                    </form>

                    @endforeach
                </div>
            </div>
        </div>
    </div>

</main>

@endsection