@extends('layouts.user_type.auth')

@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card p-4">
                    <div class="col-sm mb-4">
                        <h6>Pelatihan Saya</h6>
                    </div>
                    {{auth()->user()->level}}
                    <?php 
                    if(auth()->user()->level=="admin"){
                        $pelatihans = DB::table('pelatihans')
                        ->join('pengajars', 'pelatihans.kd_pengajar','=','pengajars.id')
                        ->join('pembayarans', 'pelatihans.id','=','pembayarans.kd_pelatihan')
                        ->where('pembayarans.id', '=', $pelatihan)
                        ->get(); 
                    } else {
                        $pelatihans = DB::table('pelatihans')
                        ->join('pengajars', 'pelatihans.kd_pengajar','=','pengajars.id')
                        ->join('pembayarans', 'pelatihans.id','=','pembayarans.kd_pelatihan')
                        ->where('pelatihans.id', '=', $pelatihan)
                        ->where('pembayarans.kd_pelajar', '=', auth()->user()->id)
                        ->get(); }?>
                    

                    @foreach ($pelatihans as $pembayaran)   

                    <form action="{{ route('kelasPelajar.store',$pembayaran->id) }}" method="POST" enctype="multipart/form-data"> 
                    @csrf
                        <div class="row">
                            <div class="col-sm">
                                <label>Nama Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="nm_pelatihan" value="{{ $pembayaran->nm_pelatihan }}" readonly>

                                <label>Nama Pengajar</label>
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan" value="{{ $pembayaran->nm_pengajar }}" readonly>

                                <label>Biaya Pelatihan</label>
                                <input type="text" class="form-control mb-3" placeholder="Biaya Pelatihan" name="biaya_pelatihan" value="{{ $pembayaran->biaya_pelatihan }}" readonly>
                            </div>
                            <div class="col-sm">
                                <label>Jadwal</label>
                                <input type="date" class="form-control mb-3" placeholder="jadwal" name="jadwal" value="{{ $pembayaran->jadwal }}" readonly>
                                
                                <label>Gambar</label>
                                <input type="file" name="image" class="form-control mb-3" placeholder="image" readonly>
                                <input type="hidden" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pelatihan" value="{{ $pembayaran->id }}" readonly>
                                <input type="hidden" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pelajar" value="{{ auth()->user()->id }}" readonly>
                                <input type="hidden" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pengajar" value="{{$pembayaran->kd_pengajar }}" readonly>
                                <input type="hidden" class="form-control mb-3" placeholder="Nama Pelatihan" name="status" value="gagal" readonly>
                            </div>
                    </form>
                    <!-- <?php print_r($pembayaran); ?> -->
                    <!-- {{$pembayaran->id}} -->
                    <?php if(auth()->user()->level == "admin"){ ?> 
                                <form action="{{ route('detailNilai.update', $pembayaran->id) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <label>Sertifikat</label><br>
  <input type="file" name="sertifikat" class="form-control" placeholder="sertifikats">
  <div class="text-center">
    <button type="submit" class="btn btn-dark w-100 mt-4 mb-0" >Upload</button>
  </div>
</form>
                            <?php } else { ?>
                                <img src="/image/{{ $pembayaran->sertifikat }}" class="p-3" width="200px">
                                <div class="text-center">
                                <a href="/image/{{ $pembayaran->sertifikat }}"  class="btn btn-dark w-100 mt-4 mb-4 mb-0" download>Download Sertifikat</a>
                                <a >Anda dapat mengambil sertifikat yg asli di kantor cabang terdekat</a>
                        </div>
                            <?php } ?>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>

</main>

@endsection