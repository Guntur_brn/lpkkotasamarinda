@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Daftar Nilai </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Pengajar</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nilai</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $pembayarans = DB::table('nilais')
                        ->join('jadwals', 'nilais.kd_sub', '=', 'jadwals.id')
                        ->join('pelajars', 'nilais.kd_pelajar', '=', 'pelajars.kd_pelajar')
                        ->select('pelajars.*', 'jadwals.*', 'nilais.*')
                        ->where('nilais.kd_pelajar', '=',  auth()->user()->id)
                        ->get(); ?>

                    @foreach ($pembayarans as $pembayaran)
                        
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                <img src="/image/{{ $pembayaran->image }}" class="avatar avatar-sm me-3" alt="user1">
                            </div>
                            <?php $nm_pelatihan = DB::table('pelatihans')
                        ->join('jadwals', 'pelatihans.id', '=', 'jadwals.kd_pelatihan')
                        ->where('jadwals.id', '=', $pembayaran->kd_sub)
                        ->get(); ?>
                        @foreach ($nm_pelatihan as $nmPelatihan)
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $nmPelatihan->nm_pelatihan}} ( </h6>
                            </div>
                            @endforeach
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pembayaran->sub_pelatihan }} ) </h6>
                            </div>
                            </div>
                        </td>
                        
                        <?php $pengajars = DB::table('pelatihans')
                        ->join('pengajars', 'pelatihans.kd_pengajar', '=', 'pengajars.id')
                        ->where('pelatihans.id', '=', $pembayaran->kd_pelatihan)
                        ->get(); ?>
                
                        @foreach ($pengajars as $pengajar)
                        
                        <td class="align-middle text-center">
                            <a class="text-xs font-weight-bold mb-0" >{{ $pengajar->nm_pengajar }}</a>
                        </td>
                        <td class="align-middle text-center">
                        <a class="text-xs font-weight-bold mb-0" >{{ $pembayaran->nilai }}</a>
                        </td>
                        <td class="align-middle text-center">
                        <a class="text-xs font-weight-bold mb-0" href="{{ route('detailNilai.show',$pembayaran->kd_pelatihan) }}">Detail</a>
                        </td>
                        </tr>
                        @endforeach 
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection