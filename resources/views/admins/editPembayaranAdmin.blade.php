@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Pembayaran Berhasil </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Pelajar</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $pembayarans = DB::table('pembayarans')
                        ->join('pelatihans', 'pembayarans.kd_pelatihan', '=', 'pelatihans.id')
                        ->join('pelajars', 'pembayarans.kd_pelajar', '=', 'pelajars.kd_pelajar')
                        ->select('pembayarans.*', 'pelatihans.nm_pelatihan', 'pelajars.nm_pelajar')
                        ->where('pembayarans.id', '=', $pembayaran->id)
                        ->get(); ?>

                    @foreach ($pembayarans as $pembayaran)
                        
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                
                            </div>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pembayaran->nm_pelatihan }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center">
                            <label class="text-xs font-weight-bold mb-0" >{{ $pembayaran->nm_pelajar }}</label>
                        </td>
                        </tr>
                          
                  </tbody>
                </table>
                <h6 class="mb-0 text-sm p-3">Bukti Transfer</h6>
                <img src="/image/{{ $pembayaran->image }}" class="p-3" width="500px">

                <?php $pengajars = DB::table('pelatihans')
                        ->join('pengajars', 'pelatihans.kd_pengajar', '=', 'pengajars.id')
                        ->where('pelatihans.id', '=', $pembayaran->kd_pelatihan)
                        ->select('pelatihans.*')
                        ->get(); ?>
                
                @foreach ($pengajars as $pengajar)

                <form action="{{ route('pembayaran.update',$pembayaran->id, $pengajar->kd_pengajar) }}" method="POST" enctype="multipart/form-data"> 
                    @csrf
                    @method('PUT')
<<<<<<< HEAD
                    <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pelatihan" value="{{ $pengajar->id }}" readonly>
                    <button type="submit" id="status" name="status" value="berhasil" class=" btn btn-primary shadow-none text-dark" style="margin-left:20px;">BENAR</button>
=======
                    <input type="text" class="form-control mb-3" placeholder="Nama Pelatihan" name="kd_pengajar" value="{{ $pengajar->kd_pengajar }}" readonly>
                    <button type="submit" id="status" name="status" value="berhasil" class=" btn btn-dark shadow-none text-primary" style="margin-left:20px;">SETUJU</button>
                    <button type="submit" id="status" name="status" value="tidaksetuju" class=" btn btn-dark shadow-none text-primary" style="margin-left:20px;">TIDAK SETUJU</button>
>>>>>>> 740cc167410b68f4844385af0f4d50612c964346
                </form>
                    @endforeach
                    @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection