@extends('layouts.user_type.auth')

@section('content')

<?php 
  $dashboards = DB::table('dashboards')->get();  
?>

<div class="col-lg-12 mb-4">
    <div class="card  p-3">
    @foreach ($dashboards as $dashboard)
      <div class="overflow-hidden position-relative border-radius-lg bg-cover " style="background-image: url('/image/{{ $dashboard->image }}'); height:400px;">
      </div>
    </div>
  </div>
  {{$dashboard->id}}
<form action="{{ route('pengaturanAdmin.update', 1) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <label>Gambar</label><br>
  <input type="file" name="image" class="form-control" placeholder="images">
  <div class="text-center">
    <button type="submit" class="btn btn-dark w-100 mt-4 mb-0" >Daftar</button>
  </div>
</form>

@endforeach

@endsection

