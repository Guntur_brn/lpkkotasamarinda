@extends('layouts.user_type.auth')

@section('content')

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-6">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Pembayaran Berhasil </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Pelajar</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $pembayarans = DB::table('pembayarans')
                        ->join('pelatihans', 'pembayarans.kd_pelatihan', '=', 'pelatihans.id')
                        ->join('pelajars', 'pembayarans.kd_pelajar', '=', 'pelajars.kd_pelajar')
                        ->select('pembayarans.*', 'pelatihans.nm_pelatihan', 'pelajars.nm_pelajar')
                        ->where('pembayarans.status', '=', 'berhasil')
                        ->get(); ?>

                    @foreach ($pembayarans as $pembayaran)
                        
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                <img src="/image/{{ $pembayaran->image }}" class="avatar avatar-sm me-3" alt="user1">
                            </div>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pembayaran->nm_pelatihan }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center">
                            <label class="text-xs font-weight-bold mb-0" >{{ $pembayaran->nm_pelajar }}</label>
                        </td>
                        <td class="align-middle text-center">
                          <!-- {{$pembayaran->id}} -->
                        <a class="text-xs font-weight-bold mb-0" href="{{ route('detailNilai.show',$pembayaran->id) }}">Detail</a>
                        </td>
                        </tr>
                          
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="card mb-4">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-sm">
                        <h6>Pembayaran Tertunda </h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pelatihan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Pelajar</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $pembayarans = DB::table('pembayarans')
                        ->join('pelatihans', 'pembayarans.kd_pelatihan', '=', 'pelatihans.id')
                        ->join('pelajars', 'pembayarans.kd_pelajar', '=', 'pelajars.kd_pelajar')
                        ->select('pembayarans.*', 'pelatihans.nm_pelatihan', 'pelajars.nm_pelajar')
                        ->where('pembayarans.status', '!=', 'berhasil')
                        ->where('pembayarans.image', '!=', '')
                        ->get(); ?>

                    @foreach ($pembayarans as $pembayaran)
                        
                            <tr>
                        <td>
                            <div class="d-flex px-2 py-1">
                            <div>
                                
                            </div>
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $pembayaran->nm_pelatihan }}</h6>
                            </div>
                            </div>
                        </td>
                        <td class="align-middle text-center">
                            <label class="text-xs font-weight-bold mb-0" >{{ $pembayaran->nm_pelajar }}</label>
                        </td>
                        <td class="align-middle text-center">
                            <a class="text-suces font-weight-bold mb-0" href="{{ route('pembayaran.edit',$pembayaran->id) }}">Detail</a>
                        </td>
                        </tr>
                          
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  @endsection