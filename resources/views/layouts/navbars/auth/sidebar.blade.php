
<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
  <div class="sidenav-header">
    <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
    <a class="align-items-center d-flex m-0 navbar-brand text-wrap" href="{{ route('dashboard') }}">
        <img src="{{ asset('assets/img/logo-ct.png') }}" class="navbar-brand-img h-100" alt="...">
        <span class="ms-3 font-weight-bold">LPK Kota Samarinda</span>
    </a>
  </div>
  <hr class="horizontal dark mt-0">
  <div class="collapse navbar-collapse  w-auto" id="sidenav-collapse-main">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('dashboardAdmin') ? 'active' : '') }}" href="{{ url('dashboardAdmin') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>shop </title>
              <x-iconpark-home width="40px" height="40px"/>
            </svg>
          </div>
          <span class="nav-link-text ms-1">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('pelatihan*') ? 'active' : '') }}" href="{{ url('pelatihan') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>shop </title>
              <x-iconpark-bookmark  width="40px" height="40px" />
            </svg>
          </div>
          <span class="nav-link-text ms-1">Pelatihan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('pembayaranAdmin') ? 'active' : '') }}" href="{{ url('pembayaranAdmin') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>box-3d-50</title>
              <x-iconpark-walletone width="40px" height="40px" />
            </svg>
          </div>
          <span class="nav-link-text ms-1">Pembayaran</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('pengajar*') ? 'active' : '') }}" href="{{ url('pengajar') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <x-iconpark-peoplestwo width="40px" height="40px" />
            </svg>
          </div>
          <span class="nav-link-text ms-1">Pengajar</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('pelajar*') ? 'active' : '') }}" href="{{ url('pelajar') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <x-iconpark-people width="40px" height="40px" />
            </svg>
          </div>
          <span class="nav-link-text ms-1">Pelajar</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ (Request::is('pengaturanAdmin') ? 'active' : '') }}" href="{{ url('pengaturanAdmin') }}">
          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <x-iconpark-settingtwo width="40px" height="40px" />
            </svg>
          </div>
          <span class="nav-link-text ms-1">Pengaturan</span>
        </a>
      </li>
    </ul>
  </div>
</aside>
