<?php

namespace App\Http\Controllers;

use App\Models\Pelatihan;
use App\Models\Pembayaran;
use Illuminate\Http\Request;

class KelasPelajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelatihans = Pelatihan::latest()->paginate(5);
    
        return view('pelajars.kelasPelajar',compact('pelatihans'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengajars = Pengajar::latest()->paginate(5);
        
        return view('pelatihans.create',compact('pengajars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo $request;
        $request->validate([
            'kd_pelatihan' => 'required',
            'kd_pelajar' => 'required',
            'status' => 'required',
        ]);
  
        $input = $request->all();
    
        Pembayaran::create($input);
     
        return redirect()->route('kelas.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelatihan  $pelatihan
     * @return \Illuminate\Http\Response
     */
    public function show(Pelatihan $pelatihan)
    {
        return view('pelajars.detailKelas',compact('pelatihan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelatihan  $pelatihan
     * @return \Illuminate\Http\Response
     */
    public function edit( $pelatihan)
    {
        // echo $pelatihan;
        return view('pelajars.detailKelas',compact('pelatihan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pelatihan  $pelatihan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pembayaran $pembayaran)
    {
        echo $pembayaran;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelatihan  $pelatihan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelatihan $pelatihan)
    {
        $pelatihan->delete();
     
        return redirect()->route('pelatihan.index');
    }
}
