<?php

namespace App\Http\Controllers;

use App\Models\Pelatihan;
use App\Models\Jadwal;
use Illuminate\Http\Request;

class KelasPengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function tambahJadwal()
    {
        $data= new Jadwal;
        $data['kd_pelatihan'] = $kelasPengajar;
        $data['sub_pelatihan'] = '';
        $data['jadwal_sub'] = '';
        $data['materi'] = '';
        $data->save();
        return view('pengajars.kelasPengajar',compact('kelasPengajar'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KelasPengajar  $kelasPengajar
     * @return \Illuminate\Http\Response
     */
    public function show( $kelasPengajar)
    {
        // echo($kelasPengajar);
        return view('pengajars.kelasPengajar',compact('kelasPengajar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KelasPengajar  $kelasPengajar
     * @return \Illuminate\Http\Response
     */
    public function edit( $pelatihan)
    {
        return view('pengajars.jadwalPengajar',compact('pelatihan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KelasPengajar  $kelasPengajar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jadwal $kelasPengajar)
    {
        echo ($kelasPengajar);
        
        $request->validate([
            'sub_pelatihan' => 'required',
            'jadwal_sub' => 'required',
        ]);
  
        $input = $request->all();

        if ($file = $request->file('materi')) {
            $destinationPath = 'file/';
            $profilefile = date('YmdHis') . "." .$file->getClientOriginalName();
            $file->move($destinationPath, $profilefile);
            $input['materi'] = "$profilefile";
        }else{
            // unset($input['materi']);
        }
          
        $kelasPengajar->update($input);
    
        return redirect()->route('pelatihanPengajar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KelasPengajar  $kelasPengajar
     * @return \Illuminate\Http\Response
     */
    public function destroy(KelasPengajar $kelasPengajar)
    {
        //
    }
}
