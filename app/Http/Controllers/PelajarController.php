<?php

namespace App\Http\Controllers;

use App\Models\Pelajar;
use Illuminate\Http\Request;

class PelajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelajars = Pelajar::latest()->paginate(5);
    
        return view('pelajars.index',compact('pelajars'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pelajars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nm_pelajar' => 'required',
            'alamat' => 'required',
            'kd_pelajar' => 'required',
            'kota' => 'required',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jns_kelamin' => 'required',
            'no_telp' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        Pelajar::create($input);
     
        return redirect('/leveling');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelajar  $pelajar
     * @return \Illuminate\Http\Response
     */
    public function show(Pelajar $pelajar)
    {
        return view('pelajars.show',compact('pelajar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelajar  $pelajar
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelajar $pelajar)
    {
        return view('pelajars.edit',compact('pelajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pelajar  $pelajar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelajar $pelajar)
    {
        $request->validate([
            'nm_pelajar' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'tmpt_lahir' => 'required',
            'kd_pelajar' => 'required',
            'tgl_lahir' => 'required',
            'jns_kelamin' => 'required',
            'no_telp' => 'required',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }
          
        $pelajar->update($input);
    
        return redirect()->route('pelajar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelajar  $pelajar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelajar $pelajar)
    {
        $pelajar->delete();
     
        return redirect()->route('pelajar.index');

    }
}
