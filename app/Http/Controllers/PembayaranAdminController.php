<?php

namespace App\Http\Controllers;

use App\Models\PembayaranAdmin;
use Illuminate\Http\Request;

class PembayaranAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admins.pembayaranAdmin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PembayaranAdmin  $pembayaranAdmin
     * @return \Illuminate\Http\Response
     */
    public function show(PembayaranAdmin $pembayaranAdmin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PembayaranAdmin  $pembayaranAdmin
     * @return \Illuminate\Http\Response
     */
    public function edit(PembayaranAdmin $pembayaranAdmin)
    {
        return view('admins.editPembayaranAdmin',compact('pembayaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PembayaranAdmin  $pembayaranAdmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PembayaranAdmin $pembayaranAdmin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PembayaranAdmin  $pembayaranAdmin
     * @return \Illuminate\Http\Response
     */
    public function destroy(PembayaranAdmin $pembayaranAdmin)
    {
        //
    }
}
