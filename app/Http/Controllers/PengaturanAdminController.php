<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use Illuminate\Http\Request;

class PengaturanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admins.pengaturanAdmin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PengaturanAdmin  $pengaturanAdmin
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $pengaturanAdmin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PengaturanAdmin  $pengaturanAdmin
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $pengaturanAdmin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PengaturanAdmin  $pengaturanAdmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $pengaturanAdmin)
    {
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }

        $pengaturanAdmin->update($input);
    
        return redirect()->route('pengaturanAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PengaturanAdmin  $pengaturanAdmin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $pengaturanAdmin)
    {
        //
    }
}
