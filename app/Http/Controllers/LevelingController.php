<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LevelingController extends Controller
{
    public function home()
    {
        $level = auth()->user()->level;
        if($level == 'admin'){
            return redirect('dashboardAdmin');
        } else if($level == 'pengajar'){
            return redirect('');
        } else {
            return redirect('dashboardPelajar');
        }
    }
}
