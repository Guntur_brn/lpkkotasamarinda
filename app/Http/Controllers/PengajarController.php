<?php

namespace App\Http\Controllers;

use App\Models\Pengajar;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Users;
use Illuminate\Http\Request;

class PengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengajars = Pengajar::latest()->paginate(100);
    
        return view('pengajars.index',compact('pengajars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengajars.createAkun');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nm_pengajar' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jns_kelamin' => 'required',
            'no_telp' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        Pengajar::create($input);

        // $attributes = request()->validate([
        //     'name' => ['required', 'max:50'],
        //     'email' => ['required', 'email', 'max:50', Rule::unique('users', 'email')],
        //     'password' => ['required', 'min:5', 'max:20'],
        //     'level' => 'required',
        //     'agreement' => ['accepted']
        // ]);
        
        // $attributes['password'] = bcrypt($attributes['password'] );

        // session()->flash('success', 'Your account has been created.');
        // User::create($attributes);
     
        return view('pengajars.createAkun', compact('request'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengajar  $pengajar
     * @return \Illuminate\Http\Response
     */
    public function show(Pengajar $pengajar)
    {
        return view('pengajars.createAkun');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengajar  $pengajar
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengajar $pengajar)
    {
        return view('pengajars.edit',compact('pengajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengajar  $pengajar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengajar $pengajar)
    {
        $request->validate([
            'nm_pengajar' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jns_kelamin' => 'required',
            'no_telp' => 'required',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }
          
        $pengajar->update($input);
    
        return redirect()->route('pengajar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengajar  $pengajar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengajar $pengajar)
    {
        $pengajar->delete();
     
        return redirect()->route('pengajar.index');

    }
}
