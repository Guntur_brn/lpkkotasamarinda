<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Models\Jadwal;
use App\Models\Nilai;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pelajars.pembayaranPelajar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function show(Pembayaran $pembayaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pembayaran $pembayaran)
    {
        return view('admins.editPembayaranAdmin',compact('pembayaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pembayaran $pembayaran)
    {
        // echo $pembayaran->kd_sub;    
        $request->validate([
            'status' => 'required',
            'kd_pelatihan' => 'required',
        ]);
  
        $input = $request->all();
          
        $pembayaran->update($input);
        echo $request->kd_pelatihan;
        echo "<br>";
        $count = Jadwal::where('kd_pelatihan',$request->kd_pelatihan)->get();
        for($x=0;$x<$count->count();$x++){
            $nilai= new Nilai;
        $nilai['kd_pelajar'] = $pembayaran->kd_pelajar;
        $nilai['kd_sub'] = $count[$x]->id;
        $nilai->save();
        }

        // 
    
        return redirect()->route('pembayaranAdmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembayaran $pembayaran)
    {
        //
    }
}
