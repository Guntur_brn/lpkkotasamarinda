<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelajar extends Model
{
    use HasFactory;

    protected $fillable = [
        'nm_pelajar', 'alamat', 'kota', 'tmpt_lahir', 'tgl_lahir', 'jns_kelamin', 'no_telp', 'image', 'kd_pelajar'
    ];
}
