<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelasPengajar extends Model
{
    use HasFactory;

    protected $fillable = [
        'nm_pelatihan','nm_pengajar','biaya_pelatihan','jadwal','image'
    ];
}
