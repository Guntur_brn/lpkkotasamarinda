<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    use HasFactory;

    protected $fillable = [
        'nm_pelatihan','kd_pengajar','biaya_pelatihan','jadwal','image', 'materi'
    ];
}
