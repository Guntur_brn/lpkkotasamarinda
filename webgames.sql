/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : webgames

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 10/08/2022 17:03:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dashboard
-- ----------------------------
DROP TABLE IF EXISTS `dashboard`;
CREATE TABLE `dashboard` (
  `kode_game` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `urut` int DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of dashboard
-- ----------------------------
BEGIN;
INSERT INTO `dashboard` VALUES ('6', 'middle', 1, NULL, NULL);
INSERT INTO `dashboard` VALUES ('9', 'middle', 2, NULL, NULL);
INSERT INTO `dashboard` VALUES ('10', 'middle', 3, NULL, NULL);
INSERT INTO `dashboard` VALUES ('8', 'middle', 4, NULL, NULL);
INSERT INTO `dashboard` VALUES ('2', 'middle', 5, NULL, NULL);
INSERT INTO `dashboard` VALUES ('6', 'high', 1, NULL, NULL);
INSERT INTO `dashboard` VALUES ('9', 'high', 2, NULL, NULL);
INSERT INTO `dashboard` VALUES ('10', 'high', 3, NULL, NULL);
INSERT INTO `dashboard` VALUES ('8', 'high', 4, NULL, NULL);
INSERT INTO `dashboard` VALUES ('2', 'high', 5, NULL, NULL);
INSERT INTO `dashboard` VALUES ('6', 'Low', 1, 'https://www.w3schools.com/sql/img_innerjoin.gif', '6');
INSERT INTO `dashboard` VALUES ('9', 'Low', 2, 'https://www.w3schools.com/sql/img_innerjoin.gif', '9');
INSERT INTO `dashboard` VALUES ('10', 'Low', 3, 'https://www.w3schools.com/sql/img_innerjoin.gif', '10');
INSERT INTO `dashboard` VALUES ('8', 'Low', 4, 'https://www.w3schools.com/sql/img_innerjoin.gif', '8');
INSERT INTO `dashboard` VALUES ('2', 'Low', 5, 'https://www.w3schools.com/sql/img_innerjoin.gif', '2');
COMMIT;

-- ----------------------------
-- Table structure for games
-- ----------------------------
DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `id_game` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `genre` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `requirement` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `image` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of games
-- ----------------------------
BEGIN;
INSERT INTO `games` VALUES ('1', '1', 'a', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('5', '5', 'e', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('4', '4', 'd', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('3', '3', 'c', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('2', '2', 'b', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('6', '6', 'f', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('7', '7', 'g', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('8', '8', 'h', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('9', '9', 'i', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
INSERT INTO `games` VALUES ('10', '10', 'j', 'low', NULL, 'ini adalah keterangan dari game yg sudah dimasukkan dalam aplikasi webgames', 'https://www.w3schools.com/sql/img_innerjoin.gif');
COMMIT;

-- ----------------------------
-- Table structure for recomendation
-- ----------------------------
DROP TABLE IF EXISTS `recomendation`;
CREATE TABLE `recomendation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `kode_game` int NOT NULL,
  `game_rating` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of recomendation
-- ----------------------------
BEGIN;
INSERT INTO `recomendation` VALUES (114, 1, 1, '5\r', 'low');
INSERT INTO `recomendation` VALUES (115, 1, 2, '2\r', 'low');
INSERT INTO `recomendation` VALUES (116, 1, 3, '5\r', 'low');
INSERT INTO `recomendation` VALUES (117, 1, 4, '0', 'low');
INSERT INTO `recomendation` VALUES (118, 1, 5, '0', 'low');
INSERT INTO `recomendation` VALUES (119, 1, 6, '1', 'low');
INSERT INTO `recomendation` VALUES (120, 1, 7, '0\r', 'low');
INSERT INTO `recomendation` VALUES (121, 1, 8, '2\r', 'low');
INSERT INTO `recomendation` VALUES (122, 1, 9, '1', 'low');
INSERT INTO `recomendation` VALUES (123, 1, 10, '0', 'low');
INSERT INTO `recomendation` VALUES (124, 2, 1, '0\r', 'low');
INSERT INTO `recomendation` VALUES (125, 2, 2, '2\r', 'low');
INSERT INTO `recomendation` VALUES (126, 2, 3, '2\r', 'low');
INSERT INTO `recomendation` VALUES (127, 2, 4, '2\r', 'low');
INSERT INTO `recomendation` VALUES (128, 2, 5, '1\r', 'low');
INSERT INTO `recomendation` VALUES (129, 2, 6, '0\r', 'low');
INSERT INTO `recomendation` VALUES (130, 2, 7, '2\r', 'low');
INSERT INTO `recomendation` VALUES (131, 2, 8, '1\r', 'low');
INSERT INTO `recomendation` VALUES (132, 2, 9, '0\r', 'low');
INSERT INTO `recomendation` VALUES (133, 2, 10, '0', 'low');
INSERT INTO `recomendation` VALUES (134, 3, 1, '1\r', 'low');
INSERT INTO `recomendation` VALUES (135, 3, 2, '3\r', 'low');
INSERT INTO `recomendation` VALUES (136, 3, 3, '1\r', 'low');
INSERT INTO `recomendation` VALUES (137, 3, 4, '1\r', 'low');
INSERT INTO `recomendation` VALUES (138, 3, 5, '0\r', 'low');
INSERT INTO `recomendation` VALUES (139, 3, 6, '0\r', 'low');
INSERT INTO `recomendation` VALUES (140, 3, 7, '2\r', 'low');
INSERT INTO `recomendation` VALUES (141, 3, 8, '1\r', 'low');
INSERT INTO `recomendation` VALUES (142, 3, 9, '1\r', 'low');
INSERT INTO `recomendation` VALUES (143, 3, 10, '3', 'low');
INSERT INTO `recomendation` VALUES (144, 4, 1, '0\r', 'low');
INSERT INTO `recomendation` VALUES (145, 4, 2, '1\r', 'low');
INSERT INTO `recomendation` VALUES (146, 4, 3, '5\r', 'low');
INSERT INTO `recomendation` VALUES (147, 4, 4, '2\r', 'low');
INSERT INTO `recomendation` VALUES (148, 4, 5, '2\r', 'low');
INSERT INTO `recomendation` VALUES (149, 4, 6, '3\r', 'low');
INSERT INTO `recomendation` VALUES (150, 4, 7, '1\r', 'low');
INSERT INTO `recomendation` VALUES (151, 4, 8, '1\r', 'low');
INSERT INTO `recomendation` VALUES (152, 4, 9, '0\r', 'low');
INSERT INTO `recomendation` VALUES (153, 4, 10, '0', 'low');
INSERT INTO `recomendation` VALUES (154, 5, 1, '0\r', 'low');
INSERT INTO `recomendation` VALUES (155, 5, 2, '3\r', 'low');
INSERT INTO `recomendation` VALUES (156, 5, 3, '4\r', 'low');
INSERT INTO `recomendation` VALUES (157, 5, 4, '5\r', 'low');
INSERT INTO `recomendation` VALUES (158, 5, 5, '5\r', 'low');
INSERT INTO `recomendation` VALUES (159, 5, 6, '3\r', 'low');
INSERT INTO `recomendation` VALUES (160, 5, 7, '0\r', 'low');
INSERT INTO `recomendation` VALUES (161, 5, 8, '0\r', 'low');
INSERT INTO `recomendation` VALUES (162, 5, 9, '1\r', 'low');
INSERT INTO `recomendation` VALUES (163, 5, 10, '0', 'low');
INSERT INTO `recomendation` VALUES (164, 6, 1, '3\r', 'low');
INSERT INTO `recomendation` VALUES (165, 6, 2, '4\r', 'low');
INSERT INTO `recomendation` VALUES (166, 6, 3, '4\r', 'low');
INSERT INTO `recomendation` VALUES (167, 6, 4, '3\r', 'low');
INSERT INTO `recomendation` VALUES (168, 6, 5, '0\r', 'low');
INSERT INTO `recomendation` VALUES (169, 6, 6, '0\r', 'low');
INSERT INTO `recomendation` VALUES (170, 6, 7, '5\r', 'low');
INSERT INTO `recomendation` VALUES (171, 6, 8, '0\r', 'low');
INSERT INTO `recomendation` VALUES (172, 6, 9, '1\r', 'low');
INSERT INTO `recomendation` VALUES (173, 6, 10, '1\r', 'low');
INSERT INTO `recomendation` VALUES (174, 7, 1, '3\r', 'low');
INSERT INTO `recomendation` VALUES (175, 7, 2, '5\r', 'low');
INSERT INTO `recomendation` VALUES (176, 7, 3, '0\r', 'low');
INSERT INTO `recomendation` VALUES (177, 7, 4, '5\r', 'low');
INSERT INTO `recomendation` VALUES (178, 7, 5, '0\r', 'low');
INSERT INTO `recomendation` VALUES (179, 7, 6, '1\r', 'low');
INSERT INTO `recomendation` VALUES (180, 7, 7, '2\r', 'low');
INSERT INTO `recomendation` VALUES (181, 7, 8, '1\r', 'low');
INSERT INTO `recomendation` VALUES (182, 7, 9, '3\r', 'low');
INSERT INTO `recomendation` VALUES (183, 7, 10, '0\r', 'low');
INSERT INTO `recomendation` VALUES (184, 8, 1, '4', 'low');
INSERT INTO `recomendation` VALUES (185, 8, 2, '0', 'low');
INSERT INTO `recomendation` VALUES (186, 8, 3, '4', 'low');
INSERT INTO `recomendation` VALUES (187, 8, 4, '0', 'low');
INSERT INTO `recomendation` VALUES (188, 8, 5, '5', 'low');
INSERT INTO `recomendation` VALUES (189, 8, 6, '1', 'low');
INSERT INTO `recomendation` VALUES (190, 8, 7, '2', 'low');
INSERT INTO `recomendation` VALUES (191, 8, 8, '1', 'low');
INSERT INTO `recomendation` VALUES (192, 8, 9, '1', 'low');
INSERT INTO `recomendation` VALUES (193, 8, 10, '0\r', 'low');
INSERT INTO `recomendation` VALUES (194, 9, 1, '0\r', 'low');
INSERT INTO `recomendation` VALUES (195, 9, 2, '1\r', 'low');
INSERT INTO `recomendation` VALUES (196, 9, 3, '1\r', 'low');
INSERT INTO `recomendation` VALUES (197, 9, 4, '2\r', 'low');
INSERT INTO `recomendation` VALUES (198, 9, 5, '3\r', 'low');
INSERT INTO `recomendation` VALUES (199, 9, 6, '1\r', 'low');
INSERT INTO `recomendation` VALUES (200, 9, 7, '4\r', 'low');
INSERT INTO `recomendation` VALUES (201, 9, 8, '5\r', 'low');
INSERT INTO `recomendation` VALUES (202, 9, 9, '0\r', 'low');
INSERT INTO `recomendation` VALUES (203, 9, 10, '0\r', 'low');
INSERT INTO `recomendation` VALUES (204, 10, 1, '1\r', 'low');
INSERT INTO `recomendation` VALUES (205, 10, 2, '0\r', 'low');
INSERT INTO `recomendation` VALUES (206, 10, 3, '2\r', 'low');
INSERT INTO `recomendation` VALUES (207, 10, 4, '4\r', 'low');
INSERT INTO `recomendation` VALUES (208, 10, 5, '3\r', 'low');
INSERT INTO `recomendation` VALUES (209, 10, 6, '2\r', 'low');
INSERT INTO `recomendation` VALUES (210, 10, 7, '1\r', 'low');
INSERT INTO `recomendation` VALUES (211, 10, 8, '0\r', 'low');
INSERT INTO `recomendation` VALUES (212, 10, 9, '5\r', 'low');
INSERT INTO `recomendation` VALUES (213, 10, 10, '0', 'low');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'aldio', 'aldio', 'aldio', 'user');
INSERT INTO `users` VALUES (2, 'ade', 'ade', 'ade', 'admin');
INSERT INTO `users` VALUES (3, 'agung', '', '', '');
INSERT INTO `users` VALUES (4, 'teddy', '', '', '');
INSERT INTO `users` VALUES (5, 'fariz', '', '', '');
INSERT INTO `users` VALUES (6, '', '', '', '');
INSERT INTO `users` VALUES (7, '', '', '', '');
INSERT INTO `users` VALUES (8, '', '', '', '');
INSERT INTO `users` VALUES (9, '', '', '', '');
INSERT INTO `users` VALUES (10, '', '', '', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
