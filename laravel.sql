/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : laravel

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 09/08/2022 15:17:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dashboards
-- ----------------------------
DROP TABLE IF EXISTS `dashboards`;
CREATE TABLE `dashboards` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dashboards
-- ----------------------------
BEGIN;
INSERT INTO `dashboards` VALUES (1, '20220801150644.jpeg', NULL, '2022-08-01 15:06:44');
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_07_24_075803_create_pelajars_table', 1);
INSERT INTO `migrations` VALUES (6, '2022_07_26_000536_create_pelajarans_table', 2);
INSERT INTO `migrations` VALUES (7, '2022_07_26_043204_create_pelajarans_table', 3);
INSERT INTO `migrations` VALUES (8, '2022_07_26_050933_create_products_table', 4);
INSERT INTO `migrations` VALUES (9, '2022_07_26_051542_create_products_table', 5);
INSERT INTO `migrations` VALUES (10, '2022_07_26_065001_create_banners_table', 6);
INSERT INTO `migrations` VALUES (11, '2022_07_26_073452_create_banners_table', 7);
INSERT INTO `migrations` VALUES (12, '2022_07_26_081451_create_pengajars_table', 8);
INSERT INTO `migrations` VALUES (13, '2022_07_29_145604_add_level_to_user_table', 8);
INSERT INTO `migrations` VALUES (15, '2022_07_27_154313_create_pengajars_table', 9);
INSERT INTO `migrations` VALUES (16, '2022_07_28_061619_create_pelatihans_table', 10);
INSERT INTO `migrations` VALUES (17, '2022_07_28_071229_create_pelajars_table', 11);
INSERT INTO `migrations` VALUES (18, '2022_07_29_072517_create_kelass_table', 12);
INSERT INTO `migrations` VALUES (19, '2022_07_29_115826_create_pembayarans_table', 13);
INSERT INTO `migrations` VALUES (20, '2022_07_29_121615_create_nilais_table', 14);
INSERT INTO `migrations` VALUES (21, '2022_07_30_065354_create_dashboards_table', 15);
COMMIT;

-- ----------------------------
-- Table structure for nilais
-- ----------------------------
DROP TABLE IF EXISTS `nilais`;
CREATE TABLE `nilais` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `kd_pelatihan` bigint DEFAULT NULL,
  `kd_pelajar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_pengajar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of nilais
-- ----------------------------
BEGIN;
INSERT INTO `nilais` VALUES (9, 9, '11', NULL, '100', '2022-08-07 04:41:23', '2022-08-07 04:42:20');
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pelajars
-- ----------------------------
DROP TABLE IF EXISTS `pelajars`;
CREATE TABLE `pelajars` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nm_pelajar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jns_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kd_pelajar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pelajars
-- ----------------------------
BEGIN;
INSERT INTO `pelajars` VALUES (10, 'mgunturborneo', 'samarinda', 'samarinda', 'samarinda', '2022-08-17', 'L', '08135060281', '20220802124845.jpg', '2022-08-02 12:48:45', '2022-08-02 12:48:45', '11');
INSERT INTO `pelajars` VALUES (11, 'borneomguntur', 'Bengkuring\r\nBengkuring', 'Samarinda', 'samarinda', '2022-08-11', 'L', '085224302550', '20220806022905.png', '2022-08-06 02:29:05', '2022-08-06 02:29:05', '12');
COMMIT;

-- ----------------------------
-- Table structure for pelatihans
-- ----------------------------
DROP TABLE IF EXISTS `pelatihans`;
CREATE TABLE `pelatihans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nm_pelatihan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_pengajar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya_pelatihan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jadwal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `materi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pelatihans
-- ----------------------------
BEGIN;
INSERT INTO `pelatihans` VALUES (9, 'Belajar Mengetik 10 Jari Tanpa Melihat', '10', 'Rp. 200.000,-', '2022-08-18', '20220801062436.png', '2022-08-01 06:24:36', '2022-08-07 07:06:22', '20220807070622.8. BAB IV.docx');
COMMIT;

-- ----------------------------
-- Table structure for pembayarans
-- ----------------------------
DROP TABLE IF EXISTS `pembayarans`;
CREATE TABLE `pembayarans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kd_pelajar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_pelatihan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pembayarans
-- ----------------------------
BEGIN;
INSERT INTO `pembayarans` VALUES (21, '20220807044059.png', '2022-08-07 04:40:46', '2022-08-07 06:07:17', '11', '9', 'berhasil', '20220807060717.png');
INSERT INTO `pembayarans` VALUES (22, '20220807044059.png', '2022-08-07 04:40:46', '2022-08-07 04:41:23', '11', '9', 'berhasil', '20220801150644.jpeg');
COMMIT;

-- ----------------------------
-- Table structure for pengajars
-- ----------------------------
DROP TABLE IF EXISTS `pengajars`;
CREATE TABLE `pengajars` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nm_pengajar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jns_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pengajars
-- ----------------------------
BEGIN;
INSERT INTO `pengajars` VALUES (10, 'Sugeng Hartato', 'Jln. Kehewanan No. 21', 'Samarinda', 'samarinda', '1988-10-05', 'L', '081350201991', '20220801060843.jpg', '2022-08-01 06:08:43', '2022-08-01 06:08:43');
INSERT INTO `pengajars` VALUES (48, 'wahyuni', 'samarinda', 'samarinda', 'samarinda', '2022-08-10', 'P', '08135060', '20220802093445.jpg', '2022-08-02 09:34:45', '2022-08-02 09:34:45');
INSERT INTO `pengajars` VALUES (49, 'wahyuni', 'samarinda', 'samarinda', 'samarinda', '2022-08-10', 'P', '08135060', '20220802093617.jpg', '2022-08-02 09:36:17', '2022-08-02 09:36:17');
INSERT INTO `pengajars` VALUES (50, 'wahyuni', 'samarinda', 'samarinda', 'samarinda', '2022-08-10', 'P', '08135060', '20220802093633.jpg', '2022-08-02 09:36:33', '2022-08-02 09:36:33');
INSERT INTO `pengajars` VALUES (51, 'wahyuni', 'samarinda', 'samarinda', 'samarinda', '2022-08-10', 'P', '08135060', '20220802093712.jpg', '2022-08-02 09:37:12', '2022-08-02 09:37:12');
INSERT INTO `pengajars` VALUES (52, 'wahyuni', 'samarinda', 'samarinda', 'samarinda', '2022-08-10', 'P', '08135060', '20220802093731.jpg', '2022-08-02 09:37:31', '2022-08-02 09:37:31');
INSERT INTO `pengajars` VALUES (53, 'azam', 'samarinda', 'samarinda', 'samarinda', '2022-08-18', 'L', '0813', '20220806022948.png', '2022-08-06 02:29:48', '2022-08-06 02:29:48');
INSERT INTO `pengajars` VALUES (54, 'azam', 'samarinda', 'samarinda', 'samarinda', '2022-08-18', 'L', '0813', '20220806023206.png', '2022-08-06 02:32:06', '2022-08-06 02:32:06');
INSERT INTO `pengajars` VALUES (55, 'azam', 'samarinda', 'samarinda', 'samarinda', '2022-08-18', 'L', '0813', '20220806023726.png', '2022-08-06 02:37:26', '2022-08-06 02:37:26');
INSERT INTO `pengajars` VALUES (56, 'azam', 'samarinda', 'samarinda', 'samarinda', '2022-08-18', 'L', '0813', '20220806023807.png', '2022-08-06 02:38:07', '2022-08-06 02:38:07');
INSERT INTO `pengajars` VALUES (57, 'azam', 'samarinda', 'samarinda', 'samarinda', '2022-08-18', 'L', '0813', '20220806023839.png', '2022-08-06 02:38:39', '2022-08-06 02:38:39');
INSERT INTO `pengajars` VALUES (58, 'juanda', 'samarinda', 'samarinda', 'samarinda', '2022-08-11', 'L', '0813', '20220809065436.png', '2022-08-09 06:54:36', '2022-08-09 06:54:36');
INSERT INTO `pengajars` VALUES (59, 'saputra', 'samarinda', 'samarinda', 'samarinda', '2022-08-17', 'L', '085224302550', '20220809065943.png', '2022-08-09 06:59:43', '2022-08-09 06:59:43');
COMMIT;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kd_pengajar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'guntur', 'guntur@gmail.com', '$2y$10$aDZuP2.3WO36JY.xgO5Gku86bw/ky85E.um7Sq3ZdrxfK7juDKRPW', NULL, '2022-07-24 13:10:38', '2022-07-24 13:10:38', 'admin', NULL);
INSERT INTO `users` VALUES (10, 'Sugeng Hartato', 'sugeng@gmail.com', '$2y$10$aDZuP2.3WO36JY.xgO5Gku86bw/ky85E.um7Sq3ZdrxfK7juDKRPW', NULL, '2022-08-01 06:09:31', '2022-08-01 06:09:31', 'pengajar', '10');
INSERT INTO `users` VALUES (11, 'mgunturborneo', 'mgunturborneo@gmail.com', '$2y$10$aDZuP2.3WO36JY.xgO5Gku86bw/ky85E.um7Sq3ZdrxfK7juDKRPW', NULL, '2022-08-02 12:46:06', '2022-08-02 12:46:06', 'pelajar', NULL);
INSERT INTO `users` VALUES (12, 'borneomguntur', 'borneomguntur0@gmail.com', '$2y$10$x3ias9ZvLnIH7pGE.ENhXu9by0AI5Z1N2mYWmtfIoHcdAtpHf/v2.', NULL, '2022-08-06 02:27:44', '2022-08-06 02:27:44', 'pelajar', NULL);
INSERT INTO `users` VALUES (13, 'azamuddin', 'azam@gmail.com', '$2y$10$5Z4.gQGzrJBCWKqaLFnSguOjDHF0tqbVUjvhdRQAaB6PaVNU.toT2', NULL, '2022-08-06 02:38:01', '2022-08-06 02:38:01', 'pelajar', NULL);
INSERT INTO `users` VALUES (14, 'congormu', 'congormu@gmail.com', '$2y$10$4FWKUoNkTh62CpzBZJ5b3u7LZ2Bw3/lHDdoXJpjiwb9H4JKkBc7K2', NULL, '2022-08-06 02:38:53', '2022-08-06 02:38:53', 'pelajar', NULL);
INSERT INTO `users` VALUES (15, 'congormu123', 'congormu123@gmail.com', '$2y$10$GvmshIXSPDd6f3XRdjdax.7ugaBVaPgEriWs0y5AzgrIpG0ORwHzy', NULL, '2022-08-06 02:40:25', '2022-08-06 02:40:25', 'pelajar', NULL);
INSERT INTO `users` VALUES (16, 'juanda', 'juanda@gmail.com', '$2y$10$VgdtlHajGhIvPWO7rCInhOx5Oy2QIAaMTcmxyZ05/Ls6g47m9y.yC', NULL, '2022-08-09 06:54:46', '2022-08-09 06:54:46', 'pelajar', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
