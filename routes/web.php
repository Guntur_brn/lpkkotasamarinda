<?php

use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InfoUserController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ResetController;
use App\Http\Controllers\SessionsController;

use App\Http\Controllers\PelajarControllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PengajarController;
use App\Http\Controllers\PengajarAkunController;
use App\Http\Controllers\PelatihanController;
use App\Http\Controllers\PelajarController;
use App\Http\Controllers\levelingController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\KelasPelajarController;
use App\Http\Controllers\PembayaranAdminController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\pelajarPembayaranController;
use App\Http\Controllers\pengaturanAdminController;
use App\Http\Controllers\KelasPengajarController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\DetailNilaiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function () {

	Route::resource('pengajar', PengajarController::class);
	Route::resource('pengajarAkun', PengajarAkunController::class);
	Route::resource('pelatihan', PelatihanController::class);
	Route::resource('pelajar', PelajarController::class);
	Route::resource('kelas', KelasController::class);
	Route::resource('kelasPelajar', KelasPelajarController::class);
	Route::resource('pembayaranAdmin', PembayaranAdminController::class);
	Route::resource('pembayaran', PembayaranController::class);
	Route::resource('pelajarPembayaran', pelajarPembayaranController::class);
	Route::resource('pengaturanAdmin', pengaturanAdminController::class);
	Route::resource('kelasPengajar', KelasPengajarController::class);
	Route::resource('jadwal', jadwalController::class);
	Route::resource('nilai', NilaiController::class);
	Route::resource('detailNilai', DetailNilaiController::class);

	Route::get('leveling', [LevelingController::class, 'home']);

	Route::get('dashboardAdmin', function () {
		return view('dashboardAdmin');
	})->name('dashboardAdmin');

	Route::get('dashboardPengajar', function () {
		return view('dashboardPengajar');
	})->name('dashboardPengajar');

	Route::get('dashboardPelajar', function () {
		return view('dashboardPelajar');
	})->name('dashboardPelajar');

	Route::get('pelatihanPengajar', function () {
		return view('pengajars/pelatihanPengajar');
	})->name('pelatihanPengajar');
	
    Route::get('/', [HomeController::class, 'home']);
	Route::get('dashboard', function () {
		return view('dashboard');
	})->name('dashboard');

	Route::get('billing', function () {
		return view('billing');
	})->name('billing');	

	Route::get('profile', function () {
		return view('profile');
	})->name('profile');

	Route::get('rtl', function () {
		return view('rtl');
	})->name('rtl');

	Route::get('user-management', function () {
		return view('laravel-examples/user-management');
	})->name('user-management');

	Route::get('tables', function () {
		return view('tables');
	})->name('tables');

    Route::get('virtual-reality', function () {
		return view('virtual-reality');
	})->name('virtual-reality');

    Route::get('static-sign-in', function () {
		return view('static-sign-in');
	})->name('sign-in');

    Route::get('static-sign-up', function () {
		return view('static-sign-up');
	})->name('sign-up');

    Route::get('/logout', [SessionsController::class, 'destroy']);
	Route::get('/user-profile', [InfoUserController::class, 'create']);
	Route::post('/user-profile', [InfoUserController::class, 'store']);
    Route::get('/login', function () {
		return view('dashboard');
	})->name('sign-up');
});



Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', [RegisterController::class, 'create']);
    Route::post('/register', [RegisterController::class, 'store']);
    Route::get('/login', [SessionsController::class, 'create']);
    Route::post('/session', [SessionsController::class, 'store']);
	Route::get('/login/forgot-password', [ResetController::class, 'create']);
	Route::post('/forgot-password', [ResetController::class, 'sendEmail']);
	Route::get('/reset-password/{token}', [ResetController::class, 'resetPass'])->name('password.reset');
	Route::post('/reset-password', [ChangePasswordController::class, 'changePassword'])->name('password.update');

});

Route::get('/login', function () {
    return view('session/login-session');
})->name('login');